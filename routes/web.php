<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* NOTE Ruta general */
Route::post('/', 'GeneralController@vistaInicial');

/* NOTE Rutas para saber que se debe renderizar en la vista principal */
Route::post('vistaAltas', 'GeneralController@vistaAltas');
Route::post('vistaConsultas', 'GeneralController@vistaConsultas');
Route::post('vistaReportes', 'GeneralController@vistaReportes');

/* NOTE Rutas que tienen contacto directo con la base de datos */
Route::post('nuevaAlta', 'GeneralController@nuevaAlta');
Route::post('consultaVisitantes', 'GeneralController@consultaVisitantes');

//NOTE Ruta que se ejecuta cuando por defecto se escribe una oración mal en la url despues de la diagonal final.
Route::fallback('GeneralController@vistaInicial');