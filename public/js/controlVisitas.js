/* INDEX Controlador principal del proyecto */
/* SECTION Importación de los componentes que se ejecutan en la vista principal*/
import menuComp from "./components/menu";
import altasComp from "./components/altas";
import consultasComp from "./components/consultas";
import reportesComp from "./components/reportes";
import modalComp from "./components/modal";
/* !SECTION  */

// NOTE Bus que controla todo el paso de variables entre los componentes
export const EventsBus = new Vue();

/* SECTION Instacia principal */
let instaciaPrincipal = new Vue({
    el: 'main',
    props: [],
    data() {
        return {
            mostrarMenu: true,
            mostrarAltas: false,
            mostrarConsultas: false,
            mostrarReportes: false,
            mostrarModal: false,
        }
    },
    components: {
        menuComp,
        altasComp,
        consultasComp,
        reportesComp,
        modalComp
    },
    mounted() {
        this.recibirParametrosVista();
        this.mostrarModalFuncion();
        this.ocultarModalFuncion();
    },
    created() {

    },
    methods: {
        /* DFUNC Función que toma el valor del parametero que le manda el componente y renderiza de acuerdo lo que recibe */
        recibirParametrosVista() {
            EventsBus.$on('vistaMostrada', (data) => {
                this.mostrarMenu = data.menu;
                this.mostrarAltas = data.altas;
                this.mostrarConsultas = data.consultas;
                this.mostrarReportes = data.reportes;
            });      
        },
        /* !DFUNC */
        
        /* DFUNC Funcion que sirve como receptor emisor de los datos del modal segun sea el caso */
        mostrarModalFuncion() {
            EventsBus.$on('altasModal', (data) => {
                this.mostrarModal = data.bandera;
            });
        },
        /* !DFUNC */

        /* DFUNC Funcion que oculta el modal cuando se apreta el boton de cerrar */
        ocultarModalFuncion() {
            EventsBus.$on('close', (data) => {
                this.mostrarModal = data.bandera;
            });
        }
        /* !DFUNC */
    }
});
/* !SECTION  */

/* !INDEX */