<!doctype html>
<html data-locale="es_MX" lang="es-MX">
  <meta charset="UTF-8" />
  <head>
    {{-- SECTION  Descripcion del meta que utiliza la vista--}}   
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0" />
    <meta property="og:title" content="Registro de visitantes"/>
    <meta charset="utf-8" />
    <meta name="description" content="Control de visitas DR" />
    <meta name="keywords" content="altas, consultas, reportes, visitas, DR" />
    <meta name="author" content="CesarMarin">
    <meta name="revisit-after" content="1 days">   
    {{-- !SECTION --}}
    @yield('metaDescription') 

    <title>Control de visitas DR</title>

    @stack('css')
    @yield('css_content')
    </head>

    <body>
        @yield('content')
    </body>


    {{-- SECTION JS --}}
    @yield('js_content')

    {{-- NOTE Librerias de VUE  --}}
        <script type="text/javascript" src="{{ asset('/js/librerias/vue.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/librerias/axios.js') }}"></script>
    {{-- !SECTION --}}
    @stack('scripts')
</html>