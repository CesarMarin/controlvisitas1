{{-- INDEX Página principal del proyecto control de visitas --}}
    @extends('layouts.codigoBase')
   
    {{-- SECTION Estilos CSS que se utilizan para el maquetado del sitio --}}
    @section('css_content')
        {{-- NOTE Estilos CSS --}}
        <link rel="stylesheet" defer type="text/css" href="{{asset('/css/minified/controlVisitas.min.css')}}" media="screen">
    @endsection
    {{-- !SECTION --}}

    @section('content')
       {{-- SECTION Maquetado de la vista principal --}}
        <main class="contenedorPrincipal grid" v-cloak>
            <modal-comp v-if="mostrarModal"></modal-comp>
            <menu-comp class="contenidoPrincipal grid" v-if="mostrarMenu"></menu-comp>
            <altas-comp class="contenidoPrincipal" v-if="mostrarAltas"></altas-comp>
            <consultas-comp class="contenidoPrincipal" v-if="mostrarConsultas"></consultas-comp>
            <reportes-comp class="contenidoPrincipal" v-if="mostrarReportes"></reportes-comp>
        </main>
        {{-- !SECTION --}}
    @endsection

    @push('scripts')
        {{-- NOTE Scripts JS --}}
        {{-- SECTION Scripts Vue que ocupa en el sitio --}}
        <script type="text/javascript" defer src="{{ asset('/js/minified/controlVisitas.min.js') }}"></script>
        {{-- !SECTION --}}
    @endpush
{{-- !INDEX --}}