<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

use App\Usuarios;
use App\Motivos;

class GeneralController extends Controller
{
    //DFUNC Ruta principal del proyecto DR
    public function vistaInicial(){
        return view('index');
    }
    //!DFUNC

    //DFUNC Ruta de altas del proyecto DR
    public function vistaAltas(){
        return json_encode('altas');
    }
    //!DFUNC

    //DFUNC Ruta de consultas del proyecto DR
    public function vistaConsultas(){
        return json_encode('consultas');
    }
    //!DFUNC

    //DFUNC Ruta de reportes del proyecto DR
    public function vistaReportes(){
        return json_encode('reportes');
    }
    //!DFUNC

    // DFUNC Ruta que inserta en la base de datos un nuevo registro de visitantes
    public function nuevaAlta(Request $request) {
        $informacionRecibida['nombre']  = $request->nombre;
        $informacionRecibida['paterno'] = $request->apellidoP;
        $informacionRecibida['materno'] = $request->apellidoM;
        $informacionRecibida['motivo']  = $request->motivo;
        //dd($informacionRecibida);

        /* NOTE Insertar un nuevo registro en la tabla usuarios */
        $newVisita                  = new Usuarios;
        $newVisita->nombre          = $informacionRecibida['nombre'];
        $newVisita->apellidop       = $informacionRecibida['paterno'];
        $newVisita->apellidom       = $informacionRecibida['materno'];
        $newVisita->estatus         = 1;
        $newVisita->save();
        
        /* NOTE Insertar el motivo */
        $newMotivo                   = new Motivos;
        $newMotivo->id_usuario       = $newVisita->id;
        $newMotivo->motivo           = $informacionRecibida['motivo'];
        $newMotivo->estatus          = 1;
        $newMotivo->save();
    }
    // !DFUNC

    // DFUNC Ruta que hace una consulta en la base de datos sobre la información de los visitantes
    public function consultaVisitantes(Request $request) {
        $informacionRecibida['nombre'] = $request->datos;
        //dd( $informacionRecibida['nombre']);

        /* NOTE Valida si se tienen parametros para hacer la consulta y si no devuelve todos los datos de la base */
        if (empty($informacionRecibida['nombre'])) {
            $consulta = Usuarios::all();
        } else {
            $consulta = Usuarios::all()->where('nombre', $informacionRecibida['nombre']);
            //$idUsuario = $consulta->id;
        }
        $respuesta["usuarios"] = $consulta;

/*         if (empty($informacionRecibida['nombre'])) {
            $consulta = Motivos::all();
        } else {
            $consulta = Motivos::where('id_usuario', $idUsuario);
        }
        $respuesta["motivos"] = $consulta; */

        return json_encode($respuesta);
    }
    // !DFUNC

}
