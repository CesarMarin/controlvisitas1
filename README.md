**Control de usuarios es un proyecto que tiene 3 funciones principales**
1.- Insertar nuevos registros en una base de datos
2.- Obtener consultas de toda la información almacenada por el sistema.
3.- Obtener un reporte de consultas.


**Instalación del proyecto**

Descargar Laragon(Windows) / docker (Mac OS) o algun otro programa que nos permita correr el proyecto Laravel en un ambiente local con una base de datos Mysql (No estoy seguro si en XAMPP funcione, pero la migración es muy sensilla, basta con cortar el proyecto y pegarlo en www del servidor local).
En Docker debes ir a preferencias y agregar la carpeta donde quieres correr tu aplicacion (C:\www\miAplicacion).

Todos los siguientes comando son sobre la ruta del servidor local

Con laragon:
C:\laragon\www

Con docker:
C:\www

Si no se tiene instalado Composer en el equipo se tiene que instalar con el comando:
composer install 
(si se instala laragon composer ya vendria instado).

Si no se tiene instalado el paquete de Laravel en el servidor hay que instalar Laravel para nuestro proyecto con el siguiente comando:

composer global require laravel/installer

Notas:

Si se instala el proyecto en Mac OS es probable que se necesite la paqueteria de "laradock" en algun momento para levantar el servidor de docker. 

En el archivo .env.example se debe quitar la extension .example y solo dejar de nombre .env, este archivo va a controlar las variables de entorno de nuestro proyecto pero no se puede subir al repositorio es por eso que se debe modificar una vez que el reporsitorio se clono.

Con esta instalación previa y con el repositorio clonado en la carpeta del servidor ya podemos correr la aplicación "controlVisitas"

Para abrir el proyecto en la url del navegador se tiene que escribir la ruta:
http://controlvisitas1.test/
Este seria el dominio local del proyecto que se clono.

Es recomendable abrir el proyecto en un editor de texto como visual studio code para observar el codigo documentado y con una buena presentación

Si se quiere editar algun componente del sistema se debe instalar npm.
NPM nos ayuda a poder minificar los componentes e incrustarlos en un unico JS

Para un ambiente de producción ejecute el siguiente comando:
npm run production

Para un ambiente de desarrollo:
npm run watch-poll

Si se quiere alterar o ver que componentes son los que se estan minificando e incrustando en el JS principal ir al archivo "webpack.mix.js"


**El diagrama relacional de la base de datos del sistema se encuentra dentro del repositorio en formato PDF**

**Creación de la base de datos**

Crear Base de datos en Mysql
query:
CREATE DATABASE controlvisitas
 
Crear Tablas de la base 
query: 
CREATE TABLE usuarios (
    id int not null auto_increment,
    nombre VARCHAR(50) not null,
    apellidoP VARCHAR(50) not null,
    apellidoM VARCHAR(50) not NULL,
    fecha date,
    estatus boolean,
    PRIMARY KEY(id)
);

CREATE TABLE motivos (
    id int not null auto_increment,
    id_usuario int not null,
    motivo varchar(1000) not null,
    fecha date,
    estatus boolean,
    PRIMARY KEY(id),
    INDEX(id_usuario),
    FOREIGN KEY(id_usuario) REFERENCES usuarios(id)
);

En el archivo .env del proyecto en la variable de entorno DB_DATABASE igualar al nombre de la base de datos
DB_DATABASE=controlvisitas


**Instalar el ORM Eloquent con nuestra base de datos**
Si se quiere crear una conexion entre eloquent y la base basta con ingresar el siguiente comando:
php artisan make:model "nombre de tu tabla"

En este caso no hace falta porque los modelos que se ocupan ya estan creados.

**ESTRUCTURA DEL PROYECTO**
En la carpeta Public se encuentra los estilos, componentes, js, minificados y recursos media que se ocuparon para el desarrollo.
En la carpeta Resourse, en la carpeta views se encuentra una base con codigo HTML5 que se extiende a la vista principal llamada 
index. El codigo base nos sirve para cumplir con el concepto de escalabilidad si el proyecto crece.
En la carpeta routes en el archivo web estan todas las comunicaciones entre el back y el front. Todas las rutas de comunicación
entre los axios del front y los controladores del back.
En la carpeta app controller se encuentra el archivo General Controller que es el que tiene las funciones que controlan el
archivo web.
