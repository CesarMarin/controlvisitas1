const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 /* NOTE Rutas de los archivos que se van a minificar y rutas de los archivos resultates que son los que se mandan a llamar en la vista */
mix.js('public/js/controlVisitas.js', 'public/js/minified/controlVisitas.min.js')
    .styles('public/css/controlVisitas.css', 'public/css/minified/controlVisitas.min.css');
